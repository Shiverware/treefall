//
//  AppDelegate.h
//  TreeFall
//
//  Created by Brett Park on 2013-01-16.
//  Copyright (c) 2013 Brett Park. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

@class TreeFallViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) TreeFallViewController *viewController;

@end
