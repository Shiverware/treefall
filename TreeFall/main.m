//
//  main.m
//  TreeFall
//
//  Created by Brett Park on 2013-01-16.
//  Copyright (c) 2013 Brett Park. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
