#import "AROverlayViewController.h"
#import <CoreMotion/CoreMotion.h>

@implementation AROverlayViewController

@synthesize captureManager;
@synthesize scanningLabel;

- (void)viewDidLoad {
    
	[self setCaptureManager:[[CaptureSessionManager alloc] init]];
    
	[[self captureManager] addVideoInput];
    
	[[self captureManager] addVideoPreviewLayer];
	CGRect layerRect = [[[self view] layer] bounds];
	[[[self captureManager] previewLayer] setBounds:layerRect];
	[[[self captureManager] previewLayer] setPosition:CGPointMake(CGRectGetMidX(layerRect),
                                                                  CGRectGetMidY(layerRect))];
	[[[self view] layer] addSublayer:[[self captureManager] previewLayer]];
    
    UIImageView *overlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlay.png"]];
    [overlayImageView setFrame:CGRectMake(0, 0, 480, 320)];
    [[self view] addSubview:overlayImageView];
    
    UIButton *overlayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [overlayButton setImage:[UIImage imageNamed:@"basebutton.png"] forState:UIControlStateNormal];
    [overlayButton setFrame:CGRectMake(130, 400, 50, 50)];
    [overlayButton addTarget:self action:@selector(scanButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:overlayButton];
    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 50, 120, 30)];
    [self setScanningLabel:tempLabel];
	[scanningLabel setBackgroundColor:[UIColor clearColor]];
	[scanningLabel setFont:[UIFont fontWithName:@"Courier" size: 18.0]];
	[scanningLabel setTextColor:[UIColor redColor]];
	[scanningLabel setText:@"Scanning..."];
    [scanningLabel setHidden:YES];
	[[self view] addSubview:scanningLabel];
    
	[[captureManager captureSession] startRunning];
    
    [UIAccelerometer sharedAccelerometer].updateInterval = 0.1;
    [UIAccelerometer sharedAccelerometer].delegate = self;
    
}

//- (void)startAnimation {
//    if (!animating) {
//        // code that configures and schedules CADisplayLink or timer here ...
//    }
//    motionManager = [[CMMotionManager alloc] init]; // motionManager is an instance variable
//    motionManager.accelerometerUpdateInterval = 0.01; // 100Hz
//    memset(filteredAcceleration, 0, sizeof(filteredAcceleration));
//    [motionManager startAccelerometerUpdates];
//}
//
//- (void)stopAnimation {
//    if (animating) {
//        // code that invalidates CADisplayLink or timer here...
//    }
//    [motionManager stopAccelerometerUpdates];
//}


- (void) scanButtonPressed {
    
    NSLog(@"Button Pressed");
    
	[[self scanningLabel] setHidden:NO];
	[self performSelector:@selector(hideLabel:) withObject:[self scanningLabel] afterDelay:2];
}

- (void)hideLabel:(UILabel *)label {
	[label setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end