//
//  ViewController.m
//  TreeFall
//
//  Created by Brett Park on 2013-01-16.
//  Copyright (c) 2013 Brett Park. All rights reserved.
//

#import "TreeFallViewController.h"
#import "AROverlayViewController.h"

@interface TreeFallViewController () {
    int _personFeet;
    int _personInches;
}
@property (strong, nonatomic) IBOutlet UIView *howTallView;
@property (assign, nonatomic) float personHeightInM;
@property (strong, nonatomic) IBOutlet UISegmentedControl *feetBarSegmentControl;
@property (strong, nonatomic) IBOutlet UILabel *inchesLabel;
@property (strong, nonatomic) IBOutlet UIStepper *inchStepper;
@property (strong, nonatomic) AROverlayViewController * arOverlayViewController;

@end

@implementation TreeFallViewController


-(AROverlayViewController *) arOverlayViewController {
    if (! _arOverlayViewController) {
        _arOverlayViewController = [AROverlayViewController new];
    }
    
    return _arOverlayViewController;
}

- (IBAction)inchHeightChanged:(UIStepper *)sender {
    self.inchesLabel.text = [NSString stringWithFormat:@"%d",(int) [sender value]];
    
}
- (IBAction)donePressed:(id)sender {
    
    _personFeet = [[self.feetBarSegmentControl titleForSegmentAtIndex:self.feetBarSegmentControl.selectedSegmentIndex] intValue];
    _personInches = [self.inchStepper value];
    
    self.personHeightInM = ((float) _personFeet * 30.f + (float)_personInches * 2.54f - 4.f * 2.54f) / 100.f;
    
    self.howTallView.hidden = YES;
    NSLog(@"This is the person Height: %f", self.personHeightInM);
    [self showCameraScreen];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void) viewWillAppear:(BOOL)animated {
    if (self.personHeightInM == 0.0f) {
        //Show the how tall bar
        self.howTallView.hidden = NO;
    } else {
        self.howTallView.hidden = YES;
        [self showCameraScreen];
    }
}

-(void) showCameraScreen {
    [self presentViewController:self.arOverlayViewController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
